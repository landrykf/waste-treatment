<?php

namespace App\Model\Capacity;

interface CompostingInterface
{
    public function composte();
}