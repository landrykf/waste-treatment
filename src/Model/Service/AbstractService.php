<?php

abstract class AbstractService 
{
    protected string $type;
    protected int $capacity;
    protected int $co2;

    public function __construct(string $type, int $capacity, int $co2)
    {
        $this->name = $type;
        $this->capacity = $capacity;
        $this->co2 = $co2;

    }
}