<?php

abstract class AbstractWaste 
{
    protected string $name;
    protected int $weight;

    public function __construct(string $name, int $weight)
    {
        $this->name = $name;
        $this->weight = $weight; 
    }
}