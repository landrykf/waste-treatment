<?php

namespace App\Model\Waste;

use AbstractService;
use AbstractWaste;
use App\Model\Capacity\IncerationInterface;
use App\Model\Capacity\RecyclingInterface;

class Glasses extends AbstractWaste

{
    public function setService(AbstractService $service)
    {
        if (!$service instanceof RecyclingInterface | !$service instanceof IncerationInterface )
        {
            throw new \Exception('Cannot assign Glasses to other services than recycling or incinerator');
        }
        
    }
}