<?php
namespace App\Model;

use AbstractService;
use AbstractWaste;

interface Co2Interface
{
    public function co2Rejected(AbstractService $sevice ,AbstractWaste $waste): int;
}